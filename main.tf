resource "google_compute_instance" "tf-chapter13" {
  # O formato que irá aparecer no dashboard do GCP é: environment-nome, exemplo: dev-tf-linux13
  count = terraform.workspace == "prd" ? 5 : 1

  name = format("%s-%s-%d", terraform.workspace, var.name, count.index)

  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.image
    }
  }

  network_interface {
    network = "default"
  }

  labels = {
    environment = terraform.workspace
    chapter     = 13
  }
}
