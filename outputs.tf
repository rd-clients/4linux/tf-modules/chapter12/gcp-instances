output "instance_id" {
  value = google_compute_instance.tf-chapter13.*.instance_id
}

output "cpu_platform" {
  value = google_compute_instance.tf-chapter13.*.cpu_platform
}
